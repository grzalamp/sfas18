﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelChooseButton : MonoBehaviour {

	[SerializeField] Text displayBest;
	[SerializeField] Image grey;
	[SerializeField] Image gold;


	public void setSelectable(bool selectable) {
		grey.enabled = !selectable;
		GetComponent<Button>().enabled = selectable;

		if (!selectable) {
			setGold(false);
			setBest(-1);
		}
	}

	public void setBest(int best) {
		if (best < 0) {
			displayBest.enabled = false;
		} else {
			displayBest.enabled = true;

			displayBest.text = "Best: " + best;
		}
	}

	public void setGold(bool g) {
		gold.enabled = g;
		gold.GetComponentInChildren<Text>().enabled = g;
	}
}
