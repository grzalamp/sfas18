﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementMenuDisplay : MonoBehaviour {

	[SerializeField] Text title, description;
	[SerializeField] Image grey;

	public void displayAchievement(Achievement achievement) {
		title.text = achievement.name;
		description.text = achievement.description;
		grey.enabled = !achievement.unlocked;
	}
}
