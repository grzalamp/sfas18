﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using System.Xml;

public class MenuManager : MonoBehaviour {

	private static MenuManager instance;

	public MenuManager Instance {
		get {
			return instance;
		}
	}

	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy(gameObject);
		}

		//DontDestroyOnLoad(gameObject);
	}

	private AssetBundle myLoadedAssetBundle;
    private string[] scenePaths;

	[SerializeField] GameObject mainScreen;
	[SerializeField] GameObject levelChooseScreen;
	[SerializeField] GameObject creditsScreen;
	[SerializeField] GameObject achievementsScreen;
	[SerializeField] GameObject levelChooseButtons;

	[SerializeField] GameObject achievementContainer;
	[SerializeField] GameObject achievementDisplayPrefab;
	[SerializeField] TextAsset achievementsXML;
	private LevelChooseButton[] chooseButtons;

	private GameObject currentScreen;
	private GameObject previousScreen;

    // Use this for initialization
    void Start()
    {
		currentScreen = mainScreen;
		previousScreen = currentScreen;
		mainScreen.SetActive(true);
		levelChooseScreen.SetActive(false);
		creditsScreen.SetActive(false);
		achievementsScreen.SetActive(false);

		chooseButtons = levelChooseButtons.GetComponentsInChildren<LevelChooseButton>();
		initLevelChooseButtons();
		initAchievements();
    }

	private void changeScreen(GameObject screen) {
		currentScreen.SetActive(false);
		previousScreen = currentScreen;
		currentScreen = screen;
		currentScreen.SetActive(true);
	}

	public void OnPlayButtonClick() {
		changeScreen(levelChooseScreen);
	}

	public void OnCreditsButtonClick() {
		changeScreen(creditsScreen);
	}

	public void OnExitButtonClick() {
		Application.Quit();
	}

	public void OnAchievementsButtonClick() {
		changeScreen(achievementsScreen);
	}

	public void OnTutorialButtonClick() {
		SceneManager.LoadScene("TutorialLevel");
	}

	public void OnBackButtonClick() {
		changeScreen(previousScreen);
	}

	public void OnChooseLevelButtonClick(int index) {
		SceneManager.LoadScene("Level" + index);
	}


	private void initLevelChooseButtons() {
		var levelsData = SaveManager.Instance.getLevelData(); 


		for (int i = 1; i < Globals.levelCount; i++) {
			var levelData = levelsData[i];
			var currentButton = chooseButtons[i-1]; //-1 because tuorial button is not in buttons array but it is in level data
			currentButton.setSelectable(false);
			if (i == 1 || levelData.finished || levelsData[i-1].finished) {
				currentButton.setSelectable(true);

				if (levelData.score > 0) {
					currentButton.setBest(levelData.score);
					currentButton.setGold(levelData.gold);
				}
			}
		}
	}

	private void initAchievements() {		
		var unlockedAchievementsNames = SaveManager.Instance.getUnlockedAchievementNames();
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(achievementsXML.text);

		var achievementDB = new List<Achievement>();
		var achievements = xmlDoc.SelectSingleNode("achievements").SelectNodes("achievement");
		foreach (XmlNode achievement in achievements) { 
			var data = new Achievement();

			var attrs = achievement.Attributes;
			data.name = attrs["name"].Value;
			data.description = achievement["description"].InnerText;

			var condition = achievement["condition"];
			var operand = condition.Attributes["operator"].Value;
			if (operand == "GREATER") {
				data.op = AchievementOperator.GREATER;
			} else if (operand == "SMALLER") {
				data.op = AchievementOperator.SMALLER;
			} else if (operand == "TRIGGER") {
				data.op = AchievementOperator.TRIGGER;
			} else if (operand == "EQUALS") {
				data.op = AchievementOperator.EQUALS;
			}
			data.conditionName = condition.Attributes["name"].Value;
			data.amount = int.Parse(condition.Attributes["amount"].Value);
			data.unlocked = unlockedAchievementsNames.Contains(data.name);

			achievementDB.Add(data);
		}

		var unlockedAchievements = new List<Achievement>();
		var lockedAchievements = new List<Achievement>();

		foreach (var ach in achievementDB) {
			if (ach.unlocked) {
				unlockedAchievements.Add(ach);
			} else {
				lockedAchievements.Add(ach);
			}
		}

		float y = 0;
		const float yoffset = 70;
		foreach  (var ach in unlockedAchievements) {
			displayAchievement(ach, y);
			y -= yoffset;
		}
		foreach  (var ach in lockedAchievements) {
			displayAchievement(ach, y);
			y -= yoffset;
		}

	}

	private void displayAchievement(Achievement a, float y) {
		var newAchi = Instantiate(achievementDisplayPrefab);
		newAchi.transform.SetParent(achievementContainer.transform);
		newAchi.transform.localPosition = new Vector3(0f, y, 0f);
		var achi = newAchi.GetComponent<AchievementMenuDisplay>();
		achi.displayAchievement(a);
	}
	
}
