﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

	[SerializeField]  float range = 2f;
	[SerializeField]  float power = 8f;
	[SerializeField]  float upForce = 4f;
	ParticleSystem[] explosions;
	bool exploded = false;

	public int damage = 50;

	private AudioSource audioSource;
	[SerializeField] AudioClip explosionSound;

	// Use this for initialization
	void Start () {
		explosions = GetComponentsInChildren<ParticleSystem>();
		audioSource = GetComponent<AudioSource>();

		explode();
	}
	
	// Update is called once per frame
	void Update () {
		if (!exploded) {
			return;
		}


		bool finished = true;
		foreach (ParticleSystem eps in explosions) {
			if (eps.isPlaying) {
				finished = false;
				break;
			}
		}

		if (finished) {
			Destroy(gameObject);
		}
	}

	public void explode() {
		// explode
		foreach (ParticleSystem eps in explosions) {
			eps.Play();
		}
		playExplosionSound();
		exploded = true;

		var hitColliders = Physics.OverlapSphere(transform.position, range);

		foreach (var collider in hitColliders) {
			// Force is applied differently if object has no rigidbody attached
			var rb = collider.GetComponent<Rigidbody>();
			if (rb != null) {
				Vector3 impulse = calculateImpulse(rb.transform);
				rb.AddForce(impulse, ForceMode.Impulse);
			}

			var pc = collider.GetComponent<PlayerController>();
			if (pc != null) {
				Vector3 impulse = calculateImpulse(pc.transform);
				impulse.y = upForce;
				pc.Knockback(impulse);
			}

			// deduct damage based on distance
			var health = collider.GetComponent<Health>();
			if (health != null) {
				if ((tag == "DamageByPlayer" && collider.tag == "Enemy") || (tag == "DamageFromEnemies" && collider.tag == "Player")) {
					var dmg = damage;
					var distance = Vector3.Distance(transform.position, collider.transform.position);
					var distanceToRangeRatio = distance/(range + range/10);
					var invRatio = 1 - distanceToRangeRatio;
					dmg = (int)(dmg*invRatio);
					if (dmg > 0) {
						health.hit(dmg);
					}
				}
			}

		}
	}

	private void playExplosionSound() {
		audioSource.PlayOneShot(explosionSound);
	}

	private Vector3 calculateImpulse(Transform other) {
		//calculate knockback direction and power
		var direction = other.transform.position - transform.position;
		var impulseDirection = new Vector3(direction.x, 0f, direction.z);
		impulseDirection.Normalize();

		var impulse = impulseDirection * power;
		var distance = Vector3.Distance(transform.position, other.position);
		if (distance <= 0) return new Vector3(0f, 0f, 0f);
		impulse /= distance;
		impulse.y = upForce;
		

		return impulse;
	}

}
