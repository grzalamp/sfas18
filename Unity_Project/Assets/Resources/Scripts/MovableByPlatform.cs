﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// MovableByPlatform can walk on a moving platform
// This object will be parented to platform's transform whilst walking on it
public class MovableByPlatform : MonoBehaviour {

    [SerializeField] float height = 1.1f;

	bool attachedToPlatform = false;
	public bool AttachedToPlatform {
		get { 
			return attachedToPlatform;
		}
	}
	Transform currentPlatform;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        ApplyFloorMovement();

	}

	void ApplyFloorMovement() {
        Ray downRay = new Ray(transform.position, -Vector3.up);
        RaycastHit hit;  
        if (Physics.Raycast(downRay, out hit)) {
            if (hit.transform != null && hit.distance < height) {
                if (hit.transform.tag == "MovingPlatform") {
					attachedToPlatform = true;
					transform.parent = hit.transform;
                } else {
					attachedToPlatform = false;
					transform.parent = null;
				}
            } else {
					attachedToPlatform = false;
					transform.parent = null;
			}
        } 
    }
}
