﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Room : MonoBehaviour {

	private Vector3 playerSpawn;
	private bool hasSeal;
	private Collider seal;

	private List<GameObject> roomEnds = new List<GameObject>();

	public Vector3 getPlayerSpawn() {
		return playerSpawn;
	}

	// Use this for initialization
	void Awake () {
		var spawn = transform.Find("Player Spawn");
		if (spawn == null) spawn = transform.Find("PlayerSpawn");
		Assert.IsNotNull(spawn);
		playerSpawn = spawn.position;

		var sealObject = transform.Find("Seal");
		if (sealObject != null) {
			hasSeal = true;
			seal = sealObject.GetComponent<Collider>();
			Assert.IsNotNull(seal);
			seal.enabled = false;
		}

		foreach(Transform child in transform) {
			if (child.gameObject.tag == "Room End") {
				roomEnds.Add(child.gameObject);
			}
		}
		
		deactivate(); //activateable objects must stay idle until player enters the room
	}

	void Start() {
	}

	public void activate() {
		foreach (var a in GetComponentsInChildren<Activateable>()) {
			a.activate();
		}

		foreach (var roomEnd in roomEnds) {
			roomEnd.GetComponentInChildren<Collider>().enabled = true;
		}
	}

	public void deactivate() {
		foreach (var a in GetComponentsInChildren<Activateable>()) {
			a.deactivate();
		}

		foreach (var roomEnd in roomEnds) {
			roomEnd.GetComponentInChildren<Collider>().enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void sealRoom() {
		// prevent player from exiting the room in unintended way
		if (hasSeal) {
			seal.enabled = true;
		}
	}

	// soft reset happens when player dies
	public void softReset() {
		var resetables = GetComponentsInChildren<Resetable>();
		foreach (var resetable in resetables) {
			resetable.reset();
		}
	}
}
