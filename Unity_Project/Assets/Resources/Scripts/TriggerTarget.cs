﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// upon calling trigger(), a relevant triggerable object will perform some action
public class TriggerTarget : MonoBehaviour {

	[SerializeField] ActionTrigerrable triggerable;

	public void trigger() {
		triggerable.trigger();
	}
}
