﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomObserver : Observer {

	int spawnersRegistered = 0;

	public RoomObserver() {
	}	
	
	public override void receive(GameObject entity, GameObject otherEntity, O_Event e) {
		switch (e) {
			case O_Event.PLAYER_REACHED_ROOM_END:
				GameManager.Instance.activateNextRoom();
				break;
				
			case O_Event.PLAYER_RESPAWNED:
				GameManager.Instance.softReset();
				break;

			case O_Event.GAME_MANAGER_POSITION_PLAYER_COROUTINE_END:
				// The programmer was dumb and he went below to take a swig at his spaghetti-o
				// So early in the morning the programmer likes his spaghetti-o
				GameManager.Instance.getCurrentRoom().sealRoom();
				break;			
				
			case O_Event.DEBUG_SKIPPED_ROOM:
				GameManager.Instance.getCurrentRoom().sealRoom();
				break;

			case O_Event.ENEMYSPAWNER_CREATED:
				spawnersRegistered++;
				break;

			case O_Event.ENEMYSPAWNER_CLEARED:
				if (--spawnersRegistered <= 0) {
					GameManager.Instance.activateNextRoom();
				}
				break;
		}
	}



}
