﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Subject {

	private Observer head;
	private int count = 0;
	public int Count {
		get {return count;}
	}

	public void add(Observer observer) {
		if (observer == null) {
			return;
		}
		observer.next = head;
		head = observer;
		++count;
	}

	public void notify(GameObject entity, GameObject otherEntity, O_Event e) {
		Observer next = head;
		while (next != null) {
			next.receive(entity, otherEntity, e);
			next = next.next;
		}
	}

	public void walkObservers() {
		Observer next = head;
		while (next != null) {
			next = next.next;
		}
	}

}
