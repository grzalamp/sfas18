﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//Linked Observer pattern
public abstract class Observer {

	public Observer next = null;

	public abstract void receive(GameObject entity, GameObject otherEntity, O_Event e);
}
