﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class DeathObserver : Observer {

	public DeathObserver() {
	}	
	
	public override void receive(GameObject entity, GameObject otherEntity, O_Event e) {
		switch (e) {
			case O_Event.PLAYER_DEATH:
            	PlayerController playerController = entity.GetComponent<PlayerController>();
				Assert.IsNotNull(playerController);
                playerController.Die();

				break;
		}
	}



}
