﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// This script manages in-game canvas
public class UIManager : MonoBehaviour
{

    // --------------------------------------------------------------

    [SerializeField] Text m_ScoreText;
    [SerializeField] Text m_TimeText;
    [SerializeField] Text m_FinalScoreText;
    [SerializeField] GameObject m_gold;
    [SerializeField] GameObject m_goldPrompt;

    [SerializeField] Slider m_PlayerHealthSlider;

    [SerializeField] GameObject HUD;
    [SerializeField] GameObject scoreScreen;
    [SerializeField] GameObject pauseScreen;

    void OnEnable()
    {
        GameManager.OnUpdateScore += UpdateCounter;
        GameManager.OnUpdateHP += UpdateHP;
        HUD.SetActive(true);
        scoreScreen.SetActive(false);
        pauseScreen.SetActive(false);
    }

    void OnDisable()
    {
        GameManager.OnUpdateScore -= UpdateCounter;
        GameManager.OnUpdateHP -= UpdateHP;
    }

    void UpdateCounter(int score)
    {
        m_ScoreText.text = "" + score;
    }

    void UpdateHP(int hp) {
        m_PlayerHealthSlider.value = (float)(hp)/100f;
    }

    public void showScoresScreen() {
        HUD.SetActive(false);
        pauseScreen.SetActive(false);
        scoreScreen.SetActive(true);

        // show final scores screen
        var score = GameManager.Instance.finalScores();
        var scoreLimit = GameManager.Instance.getGoldScoreLimit();
        m_FinalScoreText.text = "" + score;
        if (score < scoreLimit) {
            m_gold.SetActive(true);
        } else {
            m_goldPrompt.GetComponent<Text>().text = "Make it in less than " + scoreLimit + " to get the gold.";
            m_goldPrompt.SetActive(true);
        }

    }

    public void pause() {
        HUD.SetActive(false);
        scoreScreen.SetActive(false);
        pauseScreen.SetActive(true);
    }

    public void unPause() {
        HUD.SetActive(true);
        scoreScreen.SetActive(false);
        pauseScreen.SetActive(false);
    }

    public void onPressBackToMenu() {
		AchievementManager.Instance.saveStats();
		SceneManager.LoadScene("MainScene");
    }

    public void displayTime(float time) {
        //show timer on HUD
        int floored = (int)(Mathf.Floor(time));
        int minutes = floored/60;
        int seconds = floored%60;

        string text = "";
        if (minutes < 10) {
            text += "0";
        }
        text += minutes;
        text += ":";
        if (seconds < 10) {
            text += "0";
        }
        text += seconds;

        m_TimeText.text = text;
    }
}
