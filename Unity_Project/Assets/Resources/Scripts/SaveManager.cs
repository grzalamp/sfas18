﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml; 
using System.IO; 

public struct SaveLevelData {
	public int id;
	public int score;
	public bool gold;
	public bool finished;
}

public struct StatisticsData {
	public string name;
	public int amount;
}

public class SaveManager : MonoBehaviour {

	private static string[] initStatistics = {"enemies_killed", "player_deaths", "get_gold", "targets_shot"};
	private static SaveManager instance;
	public static SaveManager Instance {
		get {return instance;}
	}

	void Awake () {
		if (instance == null) {
			instance = this;
			if (!File.Exists(saveFilePath)) {
				prepareBlankSaveFile();
			}
		} else if (instance != this) {
			Destroy(gameObject);
		}

		//DontDestroyOnLoad(gameObject);
	}

	private const string saveFilePath = "saveFile.xml";

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// prepare correct xml file when no file was found
	public void prepareBlankSaveFile() {
		// Create a new file in C:\\ dir  
			XmlWriterSettings xmlWriterSettings = new XmlWriterSettings() {
				Indent = true,
				IndentChars = "\t",
				NewLineOnAttributes = true
			};
            var textWriter = XmlTextWriter.Create(saveFilePath, xmlWriterSettings);
            // Opens the document  
            textWriter.WriteStartDocument();  
				// Write first element  
				textWriter.WriteStartElement("save");  
					textWriter.WriteStartElement("levels");  
						for (int i = 0; i < Globals.levelCount; ++i) {
							textWriter.WriteStartElement("level");
								textWriter.WriteAttributeString("id", i.ToString());

								textWriter.WriteStartElement("finished");
									textWriter.WriteString("false");
								textWriter.WriteEndElement();

								textWriter.WriteStartElement("score");
									textWriter.WriteString("-1");
								textWriter.WriteEndElement();

								textWriter.WriteStartElement("gold");
									textWriter.WriteString("false");
								textWriter.WriteEndElement();

							textWriter.WriteEndElement();
						}
					textWriter.WriteEndElement();  
					
					textWriter.WriteStartElement("statistics");
						foreach (var statistic in initStatistics) {
							textWriter.WriteStartElement("statistic");
								textWriter.WriteAttributeString("name", statistic);
								textWriter.WriteAttributeString("amount", "0");
							textWriter.WriteEndElement();
						}
					textWriter.WriteEndElement(); 

					textWriter.WriteStartElement("achievements");
						textWriter.WriteStartElement("achievement");
							textWriter.WriteAttributeString("name", "blank");
						textWriter.WriteEndElement();
					textWriter.WriteEndElement();

				textWriter.WriteEndDocument(); 

            // close writer  
            textWriter.Close();  
	}

	private SaveLevelData parseLevelDataFromNode(XmlNode level) {
		var data = new SaveLevelData();
		data.id = int.Parse(level.Attributes["id"].Value);
		data.score = int.Parse(level["score"].InnerText);
		data.gold = bool.Parse(level["gold"].InnerText);
		data.finished = bool.Parse(level["finished"].InnerText);

		return data;
	}

	public Dictionary<int, SaveLevelData> getLevelData() {
		var result = new Dictionary<int, SaveLevelData>();

        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(File.ReadAllText(saveFilePath));
		var save = xmlDoc.SelectSingleNode("save");
		var levels = save.SelectSingleNode("levels").SelectNodes("level");

		for (int i = 0; i < levels.Count; ++i) {
			var level = levels[i];
			var data = parseLevelDataFromNode(level);
			var id =  data.id;

			result[id] = data;
		}

		return result;
	}

	public SaveLevelData getLevelData(int requested_id) {
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(File.ReadAllText(saveFilePath));
		var save = xmlDoc.SelectSingleNode("save");
		var levels = save.SelectSingleNode("levels").SelectNodes("level");

		for (int i = 0; i < levels.Count; ++i) {
			var level = levels[i];
			var id =  int.Parse(level.Attributes["id"].Value);
			if (id != requested_id) {
				continue;
			}

			var data = parseLevelDataFromNode(level);

			return data;
		}

		var nullData = new SaveLevelData();
		nullData.id = -1;
		return nullData;
	}

	public void saveLevel(SaveLevelData data) {
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(File.ReadAllText(saveFilePath));
		var save = xmlDoc.SelectSingleNode("save");
		var levels = save.SelectSingleNode("levels").SelectNodes("level");

		var level = levels[data.id];
		level["score"].InnerText = data.score.ToString();
		level["gold"].InnerText = data.gold.ToString();
		level["finished"].InnerText = true.ToString();


		xmlDoc.Save(saveFilePath);
	}

	public Dictionary<string, StatisticsData> getStatisticsData() {
		var result = new Dictionary<string, StatisticsData>();

        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(File.ReadAllText(saveFilePath));
		var save = xmlDoc.SelectSingleNode("save");
		var statistics = save.SelectSingleNode("statistics").SelectNodes("statistic");

		for (int i = 0; i < statistics.Count; ++i) {
			var attrs = statistics[i].Attributes;

			var name = attrs["name"].Value;
			var amount = int.Parse(attrs["amount"].Value);

			var data = new StatisticsData();
			data.name = name;
			data.amount = amount;

			result.Add(name, data);
		}

		return result;
	}

	public List<string> getUnlockedAchievementNames() {
		var result = new List<string>();
		
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(File.ReadAllText(saveFilePath));
		var save = xmlDoc.SelectSingleNode("save");
		var achievements = save.SelectSingleNode("achievements").SelectNodes("achievement");
		foreach (XmlNode achievement in achievements) {
			result.Add(achievement.Attributes["name"].Value);
		}

		return result;
	}

	public void saveUnlockAchievement(string name) {
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(File.ReadAllText(saveFilePath));
		var save = xmlDoc.SelectSingleNode("save");
		var achievement = save.SelectSingleNode("achievements");

		var node = xmlDoc.CreateNode(XmlNodeType.Element, "achievement", null);

		var attr1 = xmlDoc.CreateAttribute("unlocked");
		attr1.Value = "True";
		node.Attributes.SetNamedItem(attr1);
		var attr2 = xmlDoc.CreateAttribute("name");
		attr2.Value = name;
		node.Attributes.SetNamedItem(attr2);
		achievement.AppendChild(node);

		xmlDoc.Save(saveFilePath);
	}

	public void saveStats(Dictionary<string, StatisticsData> stats) {
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(File.ReadAllText(saveFilePath));
		var save = xmlDoc.SelectSingleNode("save");
		var xmlStats = save.SelectSingleNode("statistics").SelectNodes("statistic");

		for (int i = 0; i < xmlStats.Count; ++i) {
			XmlNode xmlStat = xmlStats[i];
			var name = xmlStat.Attributes["name"].Value;
			xmlStat.Attributes["amount"].Value = stats[name].amount.ToString();
		}


		xmlDoc.Save(saveFilePath);
	}
}
