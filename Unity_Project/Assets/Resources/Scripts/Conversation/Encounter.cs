﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ConversationSide {
	LEFT,
	RIGHT
}

public struct ConversationCharacter {
	public int id;
	public string name;
	public Sprite image;
	public ConversationSide side;
}

public struct ConversationText {
	public int characterId;
	public string text;
}

public struct ConversationEncounter {
	public Dictionary<int, ConversationCharacter> characters;
	public List<ConversationText> textList;
}