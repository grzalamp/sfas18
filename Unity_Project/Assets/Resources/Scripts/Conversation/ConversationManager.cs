﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Xml;

public class ConversationManager : MonoBehaviour {

	private static ConversationManager instance;

	public static ConversationManager Instance {
		get {
			return instance;
		}
	}

	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy(gameObject);
		}

	}

	
	[SerializeField] Canvas conversationCanvas;
	private GameObject leftImage, rightImage, conversationText;

	[SerializeField] TextAsset conversationXML;

	private XmlNodeList encounters;
	private ConversationEncounter currentEncounter;
	private int encounterIndex = 0;
	private int textIndex = 0;

	private Dictionary<string, Sprite> characterImages = new Dictionary<string, Sprite>();

	void Start() {
		Assert.IsNotNull(conversationCanvas);
		leftImage = conversationCanvas.transform.Find("ImageLeft").gameObject;
		rightImage = conversationCanvas.transform.Find("ImageRight").gameObject;
		conversationText = conversationCanvas.transform.Find("ConversationText").gameObject;

		leftImage.SetActive(false);
		rightImage.SetActive(false);
		conversationCanvas.enabled = false;

		interpretXML();
		initCharactersImages();
	}

	private void interpretXML() {
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(conversationXML.text);

		encounters = xmlDoc.GetElementsByTagName("encounter");
	}

	private void setText(string text) {
		conversationText.GetComponent<Text>().text = text;
	}

	private void initCharactersImages() {
		characterImages.Add("JOSH", Resources.Load("Images/Characters/josh", typeof(Sprite)) as Sprite);
		characterImages.Add("COOL", Resources.Load("Images/Characters/cool", typeof(Sprite)) as Sprite);
		characterImages.Add("HAT", Resources.Load("Images/Characters/hat", typeof(Sprite)) as Sprite);
		characterImages.Add("FATHER", Resources.Load("Images/Characters/father", typeof(Sprite)) as Sprite);
		characterImages.Add("ENEMY", Resources.Load("Images/Characters/enemy", typeof(Sprite)) as Sprite);
	}

	public void startNextEncounter() {
		if (encounterIndex >= encounters.Count) {
			return;
		}
		runTime(false);
		conversationCanvas.enabled = true;		
		GameManager.Instance.getPlayer().GetComponent<PlayerController>().controlsLocked = true;
		var currentEncounterXML = encounters[encounterIndex++];

		Dictionary<int, ConversationCharacter> characters = new Dictionary<int, ConversationCharacter>();
		var textList = new List<ConversationText>();

		var children = currentEncounterXML.ChildNodes;
		for (int i = 0; i < children.Count; i++) {
			var child = children[i];
			if (child.Name == "character") {
				var attributes = child.Attributes;
				int id = int.Parse(attributes["id"].Value);
				ConversationCharacter character = new ConversationCharacter();
				character.id = id;
				character.name = attributes["type"].Value;
				character.side = attributes["side"].Value == "LEFT" ? ConversationSide.LEFT : ConversationSide.RIGHT;
				character.image = characterImages[character.name];
				characters.Add(id, character);
			} else if (child.Name == "text") {
				var attributes = child.Attributes;
				int id = int.Parse(attributes["character"].Value);
				var text = child.InnerText;
				ConversationText newText;
				newText.characterId = id;
				newText.text = text;
				textList.Add(newText);
			}
		}

		currentEncounter.characters = characters;
		currentEncounter.textList = textList;

		textIndex = 0;
		displayNextText();
	}

	private void displayNextText() {
		var textList = currentEncounter.textList;
		if (textList == null || textIndex >= textList.Count) {
			runTime(true);
			conversationCanvas.enabled = false;
			GameManager.Instance.getPlayer().GetComponent<PlayerController>().controlsLocked = false;
			return;
		}

		leftImage.SetActive(false);
		rightImage.SetActive(false);

		var currentText = textList[textIndex++];
		var currentCharacter = currentEncounter.characters[currentText.characterId];

		setSprite(currentCharacter.image, currentCharacter.side);
		setText(currentText.text);

	}

	private void setSprite(Sprite sprite, ConversationSide side) {
		leftImage.SetActive(false);
		rightImage.SetActive(false);
		Image image;
		if (side == ConversationSide.LEFT) {
			image = leftImage.GetComponent<Image>();
		} else {
			image = rightImage.GetComponent<Image>();
		}

		image.gameObject.SetActive(true);
		image.sprite = sprite;
	}

	void Update() {
		if (Input.GetButtonDown("Jump") || Input.GetButtonDown("Fire1")) {
			displayNextText();
		}
	}

	private void runTime(bool run) {
		Time.timeScale = run ? 1f : 0f;
	}

}
