﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickableType {
	WEAPON,
	HAT,
}

// Pickable is an object that can be picked up by player
public class Pickable : MonoBehaviour {

	[SerializeField] GameObject weapon;
	[SerializeField] GameObject model;

	[SerializeField] PickableType type = PickableType.WEAPON;

	private bool picked = false;

	// Use this for initialization
	void Start () {
		// placeholder is useful only in editor. It should be replaced by the model of a pickable object
		Destroy(transform.Find("Placeholder").gameObject);

		GameObject gunModel = Instantiate(model) as GameObject;
		gunModel.transform.parent = transform;
		gunModel.transform.localPosition = new Vector3(0f, 0f, 0f);
		gunModel.transform.Rotate(20f, 0f, 0f);
	}
	
	// Update is called once per frame
	void Update () {
		// Animate rotating object
		transform.Rotate(0f, 60f*Time.deltaTime, 0f);
	}

	public void pick() {
		picked = true;
		Destroy(gameObject);
	}

	public bool isPicked() {
		return picked;
	}

	public GameObject getWeapon() {
		return weapon;
	}

	public PickableType getType() {
		return type;
	}
}
