﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MortarShell : MonoBehaviour {

	private Rigidbody rb;

	[SerializeField]
	GameObject model;

	[SerializeField]
	GameObject explosion;
	[SerializeField] int explosionDamage = 20;

	bool collided = false;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		//always face velocity
		transform.LookAt(transform.position - rb.velocity); 
		transform.Rotate(-90, 0 , 0);
	}

	void OnCollisionEnter(Collision other) {
		if (!collided) { // without this impact would sometimes create two explosions
			rb.isKinematic = true;
			rb.detectCollisions = false;
			GetComponent<CapsuleCollider>().enabled = false;
			model.GetComponent<MeshRenderer>().enabled = false;
			collided = true;

			GameObject newExplosion = Instantiate(explosion) as GameObject;
			newExplosion.transform.position = transform.position;
			newExplosion.transform.rotation = transform.rotation;
			newExplosion.transform.localScale = transform.localScale;
			newExplosion.tag = tag;
			newExplosion.GetComponent<Explosion>().damage = explosionDamage;
		}


		Destroy(gameObject);
	}
}
