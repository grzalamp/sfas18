﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FireCrawl : Activateable, Resetable {

	[SerializeField] float sizeX = 10;

	private GameObject deathZone;
	private BoxCollider deathZoneCollider;

	[SerializeField] GameObject firePrefab;
	[SerializeField] float maxSpread = 20f;
	private float currentSpread = 0f;

	[SerializeField] float speed = 2.5f;
	[SerializeField] float maxSpreadOffsetX = 5f;
	[SerializeField] const float maxSpreadOffsetZ = 3f;
	private float spreadOffset = 0f;

	[SerializeField] float MAX_VARIATION = 1.5f;

	private bool activated = false;

	[SerializeField] bool asWave = false;
	private const int waveFireRows = 3;
	private int rowsSpawned;
	[SerializeField] float initialDelay = 0f;
	private float initialDelayed = 0f;

	private int activations = 0;
	private List<List<GameObject>> fires = new List<List<GameObject>>();

	void OnDrawGizmosSelected() {
		Vector3 point1 = transform.position - transform.right * sizeX;
		Vector3 point2 = transform.position - transform.right * -sizeX;

		var maxZ = maxSpread;
		Vector3 point3 = transform.position + transform.forward * maxZ;
		
		Gizmos.color = Color.red;
		Gizmos.DrawLine(point1, point2);
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(transform.position, point3);
		Gizmos.DrawLine(point3, point3 + transform.right * 2f + transform.forward * -2f);
		Gizmos.DrawLine(point3, point3 + transform.right * -2f + transform.forward * -2f);
		
    }

	// Use this for initialization
	void Start () {
	}

	private void init() {
		rowsSpawned = 0;
		currentSpread = 0f;
		spreadOffset = 0f;

		deathZone = new GameObject("Death Zone");
		deathZone.transform.parent = transform;
		deathZone.transform.localPosition = Vector3.zero;
		deathZone.transform.rotation = transform.rotation;
		deathZoneCollider = deathZone.AddComponent(typeof(BoxCollider)) as BoxCollider;
		deathZoneCollider.size = new Vector3(sizeX*2f, 3f, 1f);
		deathZoneCollider.tag = "FireHazard";
		deathZoneCollider.isTrigger = true;


		initFire(maxSpreadOffsetZ+0.05f);
	}

	void initFire(float currentZ) {
		var currentFires = new List<GameObject>();

		var fire = Instantiate(firePrefab) as GameObject;
		fire.transform.parent = transform;
		fire.transform.localPosition = new Vector3(0f, 0f, currentZ);
		currentFires.Add(fire);

		var xSize = deathZoneCollider.size.x;
		var currentXOffset = 0f;
		var i = 0;
		//spread fire to left and right until limit is reached
		while (currentXOffset < xSize/4f && i < 1000) {
			currentXOffset += maxSpreadOffsetX;

			// add some randomness so the fire does not look so artificial
			var randx = Random.Range(-MAX_VARIATION, MAX_VARIATION);
			var randz = Random.Range(-MAX_VARIATION, MAX_VARIATION);

			var fire1 = Instantiate(firePrefab) as GameObject;
			fire1.transform.parent = transform;
			fire1.transform.localPosition = Vector3.zero;
			fire1.transform.localPosition += new Vector3(currentXOffset + randx, 0f, currentZ + randz);

			var fire2 = Instantiate(firePrefab) as GameObject;
			fire2.transform.parent = transform;
			fire2.transform.localPosition = Vector3.zero;
			fire2.transform.localPosition += new Vector3(-currentXOffset + randz, 0f, currentZ + randx);

			currentFires.Add(fire1);
			currentFires.Add(fire2);

			i++;
		}

		fires.Add(currentFires);
	}
	
	// Update is called once per frame
	void Update () {
		if (!activated) {
			return;
		}

		// move fires and collider. remove old fires if the fire moves as wave
		if (!asWave) {
			if (currentSpread < maxSpread) {
				var spread = speed*Time.deltaTime;
				currentSpread += spread;
				deathZoneCollider.size = deathZoneCollider.size + new Vector3(0f, 0f, spread);
				deathZone.transform.localPosition += new Vector3(0f, 0f, spread/2f);
				spreadOffset += spread;


				if (spreadOffset > maxSpreadOffsetZ) {
					spreadOffset = 0f;

					initFire(currentSpread+(maxSpreadOffsetZ/2f)+0.05f);
				}
			}
		} else {

			var spread = speed*Time.deltaTime;
			currentSpread += spread;
			deathZone.transform.localPosition += new Vector3(0f, 0f, spread);
			spreadOffset += spread;
			
			if (spreadOffset > maxSpreadOffsetZ) {
				spreadOffset = 0f;

				initFire(currentSpread+(maxSpreadOffsetZ/2f)+0.05f);
				rowsSpawned++;
				if (rowsSpawned >= waveFireRows) {
					foreach(var fire in fires[0]) {
						Destroy(fire);
					}
					fires.RemoveAt(0);
				}
			}

			if (currentSpread >= maxSpread) {
				reset(false);
			}
		}
	}

	public void reset() {
		reset(true);
	}

	private void reset(bool total) {
		if (total) {
			activations = 0;
			initialDelayed = 0f;
		}
		deactivate();
		activate();
	}

	public override void deactivate() {
		for (int i = 0; i < transform.childCount; i++) {
			Destroy(transform.GetChild(i).gameObject);
		}
		fires.Clear();

		activated = false;
	}

	public override void activate() {
		if (activations == 0) {
			StartCoroutine(activateWithDelay(initialDelay));
			return;
		}
		++activations;
		init();

		activated = true;
	}

	private IEnumerator activateWithDelay(float delay) {
		while (true) {
			initialDelayed += Time.deltaTime;

			if (initialDelayed >= delay) {
				++activations;
				init();

				activated = true;
				break;
			}
			yield return null;
		}

		yield return null;
	}

}
