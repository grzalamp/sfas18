﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireStatic : Activateable {
	private const float MAX_VARIATION = 1.5f;
	private const float distaceBetweenFires = 5f;

	[SerializeField] float sizeX;
	[SerializeField] float sizeZ;

	private GameObject deathZone;
	private BoxCollider deathZoneCollider;

	[SerializeField] GameObject firePrefab;

	private List<GameObject> fires = new List<GameObject>();
	
	void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Vector3 point1 = transform.position - new Vector3(sizeX, 0f, sizeZ);
		Vector3 point2 = transform.position - new Vector3(-sizeX, 0f, sizeZ);
		Vector3 point3 = transform.position - new Vector3(-sizeX, 0f, -sizeZ);
		Vector3 point4 = transform.position - new Vector3(sizeX, 0f, -sizeZ);
		
		Gizmos.DrawLine(point1, point2);
		Gizmos.DrawLine(point2, point3);
		Gizmos.DrawLine(point3, point4);
		Gizmos.DrawLine(point4, point1);
    }

	// Use this for initialization
	void Awake () {
		deathZone = new GameObject("Death Zone");
		deathZone.transform.parent = transform;
		deathZone.transform.localPosition = Vector3.zero;
		deathZoneCollider = deathZone.AddComponent(typeof(BoxCollider)) as BoxCollider;
		deathZoneCollider.size = new Vector3(sizeX*2f, 10f, sizeZ*2f);
		deathZoneCollider.tag = "FireHazard";
		deathZoneCollider.isTrigger = true;
		
		var z = -sizeZ;
		while (z < sizeZ) {
			initFire(z);
			z += distaceBetweenFires;
		}
	}

	private void initFire(float z) {
		var fire = Instantiate(firePrefab) as GameObject;
		fire.transform.parent = transform;
		fire.transform.localPosition = new Vector3(0f, 0f, z);
		fires.Add(fire);

		var xSize = deathZoneCollider.size.x;
		var currentXOffset = 0f;
		var i = 0;
		while (currentXOffset - (sizeX/2f) < xSize/2f && i < 1000) {
			currentXOffset += distaceBetweenFires;

			var randx = Random.Range(-MAX_VARIATION, MAX_VARIATION);
			var randz = Random.Range(-MAX_VARIATION, MAX_VARIATION);

			var fire1 = Instantiate(firePrefab) as GameObject;
			fire1.transform.parent = transform;
			fire1.transform.localPosition = Vector3.zero;
			fire1.transform.localPosition += new Vector3(currentXOffset + randx, 0f, z + randz);

			var fire2 = Instantiate(firePrefab) as GameObject;
			fire2.transform.parent = transform;
			fire2.transform.localPosition = Vector3.zero;
			fire2.transform.localPosition += new Vector3(-currentXOffset + randz, 0f, z + randx);

			fires.Add(fire1);
			fires.Add(fire2);
		
			i++;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void activate() {
		deathZoneCollider.enabled = true;
		foreach (var fire in fires) {
			fire.SetActive(true);
		}
	}	
	
	public override void deactivate() {
		deathZoneCollider.enabled = false;
		foreach (var fire in fires) {
			fire.SetActive(false);
		}
	}
}
