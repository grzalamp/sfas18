﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Mortar : Activateable {

	private static float G = Mathf.Abs(Physics.gravity.y);
	private GameObject target;

	[SerializeField]
	float power;

	[SerializeField]
	float cooldown;

	[SerializeField]
	GameObject ammunition;

	private AudioSource audioSource;
	[SerializeField] AudioClip shotSound;

	private float maxRange;

	void OnDrawGizmosSelected() {
		var optAngle = Mathf.PI/4;
		var range = (power*power*Mathf.Sin(2f*optAngle))/G;

		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position, range);
	}

	// Use this for initialization
	void Start () {
		target = GameManager.Instance.getPlayer();
		audioSource = GetComponent<AudioSource>();

		StartCoroutine(Attack());

		var optAngle = Mathf.PI/4;
		maxRange = (power*power*Mathf.Sin(2f*optAngle))/G;
	}

	private static float calculateAngle(float power, Vector3 source, Vector3 target) {
		//given initial speed and distance, calculate angle needed to reach target
		Vector3 relativePos = target - source;
		float x = Mathf.Sqrt(relativePos.x*relativePos.x + relativePos.z*relativePos.z);
		float y = relativePos.y;

		//not possible to calculate
		if (x <= 0.0) return -100;
		else if (power <= 0.0) return -100;

		float vsquared = power * power;
		float delta = (power*power*power*power) - G*(G*(x*x) + 2*(y*vsquared));
		float rootdelta = Mathf.Sqrt(delta);
		if (delta < 0) return -100;

		float a = Mathf.Atan((vsquared + rootdelta) / (G*x));
		float b = Mathf.Atan((vsquared - rootdelta) / (G*x));

		float angle = Mathf.Max(a, b);

		return angle;
	}
	
	// Update is called once per frame
	void Update () {
		var player = GameManager.Instance.getPlayer();

		if (Vector3.Distance(player.transform.position, transform.position) < maxRange) {
			var targetVec = player.transform.position;
			targetVec.y = transform.position.y;
			transform.rotation = Quaternion.LookRotation(targetVec - transform.position);
		}
	}

	IEnumerator Attack() {

		float shootAngle = calculateAngle(power, transform.position, target.transform.position);

		// valid angle was calculated if returned angle is bigger than -3.14
		if (shootAngle > -Mathf.PI) {
			GameObject bullet = Instantiate(ammunition) as GameObject;
			bullet.transform.position = transform.position;
			bullet.tag = tag;

			Vector2 impulse = new Vector2(power*Mathf.Cos(shootAngle), power*Mathf.Sin(shootAngle));

			Vector3 v = (target.transform.position - transform.position).normalized * impulse.x;
			v.y = impulse.y;
			
			bullet.GetComponent<Rigidbody>().AddRelativeForce(v, ForceMode.Impulse);

			MovableByPlatform mv = target.GetComponent<MovableByPlatform>();
			if (mv != null && mv.AttachedToPlatform) {
				bullet.transform.parent = mv.transform.parent;
			}

			playShootSound();
		}

		yield return new WaitForSeconds(cooldown);
		yield return null;
		StartCoroutine(Attack());
	}

	private void playShootSound() {
		audioSource.PlayOneShot(shotSound);
	}

	public override void activate() {
		enabled = true;
	}

	public override void deactivate() {
		enabled = false;
	}
}
