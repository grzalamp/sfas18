﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RisingWater : Activateable, Resetable {

	[SerializeField] GameObject waterPlatform, waterDaytime;
	[SerializeField] float maxClimb;
	private float climbed = 0f;
	[SerializeField] float speed = 5f;

	private bool activated = false;
	private Vector3[] startPoss = new Vector3[2];
	[SerializeField] float initialDelay;
	private float delayed = 0f;


	void OnDrawGizmosSelected() {
		var maxY = maxClimb;
		Vector3 point3 = transform.position + transform.up * maxY;
		
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(transform.position, point3);
		Gizmos.DrawLine(point3, point3 + transform.right * 2f + (-transform.up) * 2f);
		Gizmos.DrawLine(point3, point3 + (-transform.right) * 2f + (-transform.up) * 2f);
		
    }
	
	void Start() {
		startPoss[0] = waterPlatform.transform.position;
		startPoss[1] = waterDaytime.transform.position;
	}

	// Update is called once per frame
	void Update () {
		if (activated && climbed < maxClimb) {
			if (delayed < initialDelay) {
				delayed += Time.deltaTime;
				return;
			}

			var step = speed*Time.deltaTime;
			waterPlatform.transform.localScale += new Vector3(0f, step, 0f);
			waterPlatform.transform.position += new Vector3(0f, step/2f, 0f);

			var y = waterPlatform.transform.position.y + (waterPlatform.GetComponent<Collider>().bounds.size.y/2f);
			var oldPos = waterDaytime.transform.position;
			waterDaytime.transform.position = new Vector3(oldPos.x, y, oldPos.z);

			climbed += step;
		}
	}

	public void reset() {
		deactivate();
		climbed = 0f;
		delayed = 0f;
		waterPlatform.transform.position = startPoss[0];
		var oldScale = waterPlatform.transform.localScale;
		waterPlatform.transform.localScale = new Vector3(oldScale.x, 1f, oldScale.z);
		waterDaytime.transform.position = startPoss[1];
		activate();
	}

	public override void activate() {
		activated = true;
	}

	public override void deactivate() {
		activated = false;
	}
}
