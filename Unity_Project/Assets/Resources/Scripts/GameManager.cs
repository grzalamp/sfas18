﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GameManager : MonoBehaviour {
	[SerializeField] private int levelNo;

	private static GameManager instance;

	private int points, currentRoomPoints = 0;
    public delegate void UpdateScore(int score);
    public static event UpdateScore OnUpdateScore;

	private int lastPlayerHp = 0;
    public delegate void UpdateHP(int score);
    public static event UpdateHP OnUpdateHP;

	private Observer roomObserver, deathObserver, achievementObserver;

	private Subject observerSubject;

	private const float playerRepositionSpeed = 2f;

	private AudioSource backgroundMusic;

	private float timePassed = 0f;

	private bool timerStarted = false;
	[SerializeField] int scoreForGold;

	public static GameManager Instance {
		get { 
			return instance;	
		}
	}

	[SerializeField] GameObject player;

	[SerializeField] Room[] rooms;
	private Room currentRoom;
	private int currentRoomIndex = -1;

	[SerializeField] Camera mainCamera;
	[SerializeField] Camera introCamera;

	[SerializeField] UIManager mainUIManager;
	private bool paused = false;


	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
			
			// prepare listeners
			observerSubject = new Subject();
			observerSubject.add(new RoomObserver());
			observerSubject.add(new DeathObserver());
			observerSubject.add(new AchievementObserver());
		} else if (instance != this) {
			Destroy(gameObject);
		}

		//DontDestroyOnLoad(gameObject);
	}

	void Start() {
		Assert.AreNotEqual(0, rooms.Length);
		Assert.IsNotNull(mainCamera);

		// prepare music
		backgroundMusic = GetComponent<AudioSource>();
		Assert.IsNotNull(backgroundMusic);
		#if UNITY_EDITOR
			backgroundMusic.Stop();
		#endif

		StartCoroutine(levelIntro());
	}

	public Subject GetSubject() {
		return observerSubject;
	}
	private IEnumerator levelIntro() {
		// show intro level animation
		mainCamera.gameObject.SetActive(false);
		introCamera.gameObject.SetActive(true);

		var introCameraScript = introCamera.GetComponent<IntroCamera>();
		while (!introCameraScript.isEnded()) {
			yield return null;
		}

		// switch cameras, start conversation
		mainCamera.gameObject.SetActive(true);
		introCamera.gameObject.SetActive(false);

		ConversationManager.Instance.startNextEncounter();
		activateNextRoom();
		timerStarted = true; //start measuring time from now
		
		yield return null;
	}

	public void softReset() {
		// happens when player dies. Points acquired in this room must be removed
		currentRoomPoints = 0;
		addPoints(0);

		currentRoom.softReset();
	}

	public void activateNextRoom(bool forceTeleport = false) {
		// activate room upon entering or finish game if no more rooms are available
		points += currentRoomPoints;
		currentRoomPoints = 0;
		if (currentRoomIndex == rooms.Length-1) {
			mainUIManager.showScoresScreen();
			gameObject.SetActive(false);
			Time.timeScale = 0f;
			observerSubject.notify(gameObject, null, O_Event.LEVEL_FINISHED);
			saveFinalLevelProgress();
			return;
		}

		if (currentRoom != null) {
			currentRoom.deactivate();
		}
		currentRoom = rooms[++currentRoomIndex];
		if (forceTeleport || currentRoomIndex == 0) {
			respawnPlayer();
		} else {
			positionPlayerInRoom(); // position player in a correct place before starting new room
		}
		player.GetComponent<Health>().restore();
		currentRoom.activate();
	}

	private void positionPlayerInRoom() {
		player.GetComponent<PlayerController>().controlsLocked = true;
		StartCoroutine(positionPlayerCoroutine());
	}

	private IEnumerator positionPlayerCoroutine() {
		var target = currentRoom.getPlayerSpawn();
		target.y = player.transform.position.y;
		var targetDirection = target - player.transform.position;
		targetDirection.Normalize();

		player.GetComponent<PlayerController>().Stop();

		while(true) {
			target.y = player.transform.position.y;
			targetDirection = target - player.transform.position;
			player.transform.position += targetDirection * playerRepositionSpeed * Time.deltaTime;
			player.transform.rotation = Quaternion.LookRotation(target - player.transform.position);

			var distance = Vector2.Distance(player.transform.position, target);

			if (distance < 0.1f) {
				break;
			}

			yield return null;
		}

		player.GetComponent<PlayerController>().controlsLocked = false;
		observerSubject.notify(gameObject, player, O_Event.GAME_MANAGER_POSITION_PLAYER_COROUTINE_END);
		yield return null;
	}

	public Room getCurrentRoom() {
		return currentRoom;
	}

	public void respawnPlayer() {
		player.transform.position = currentRoom.getPlayerSpawn();
	}

	public GameObject getPlayer() {
		return player;
	}
	
	// Update is called once per frame
	void Update () {
		// debug option. press F1 to skip to next room
		#if UNITY_EDITOR
			if (Input.GetButtonDown("SkipRoom")) {
				if (!introCamera.GetComponent<IntroCamera>().isEnded()) {
					introCamera.GetComponent<IntroCamera>().Skip();
				} else {
					activateNextRoom(true);
					observerSubject.notify(gameObject, null, O_Event.DEBUG_SKIPPED_ROOM);
				}
			}
		#endif

		// update HUD display
		var playerHP = player.GetComponent<Health>().getHealth();
		if (lastPlayerHp != playerHP) {
			lastPlayerHp = playerHP;
			if (OnUpdateHP != null)
				OnUpdateHP(playerHP);
		}

		if (timerStarted) {
			timePassed += Time.deltaTime;
			mainUIManager.displayTime(timePassed);
		}

		if (Input.GetButtonDown("Pause")) {
			pause();
		}
	}

	public void pause() {
		if (!paused) {
			player.GetComponent<PlayerController>().controlsLocked = true;
			Time.timeScale = 0f;
			mainUIManager.pause();
			paused = true;
		} else {
			player.GetComponent<PlayerController>().controlsLocked = false;
			Time.timeScale = 1f;
			mainUIManager.unPause();
			paused = false;
		}
	}

	public void addPoints(int points) {
		this.currentRoomPoints += points;
		if (OnUpdateScore != null)
			OnUpdateScore(this.points + this.currentRoomPoints);
	}


	//redundant
	public void DelegateDestroy(GameObject g) {
		Destroy(g);
	}

	// redundant
	public void DelegatePrint(string a) {
		print(a);
	}

	public int finalScores() {
		int time = (int)(Mathf.Floor(timePassed*100f));
		int points = (int)(this.points * 1f);
		return time - points;
	}

	public int getGoldScoreLimit() {
		return scoreForGold;
	}

	public int getLevelNo() {
		return levelNo;
	}

	private void saveFinalLevelProgress() {
		// upon level finish, save progress
		var oldData = SaveManager.Instance.getLevelData(levelNo);
		var newData = new SaveLevelData();
		newData.id = oldData.id;
		if (oldData.score > 0) {
			newData.score = Mathf.Min(oldData.score, finalScores());
		} else {
			newData.score = finalScores();
		}
		newData.gold = oldData.gold || (finalScores() < getGoldScoreLimit());
		newData.finished = true;

		if (newData.gold) {
			observerSubject.notify(gameObject, null, O_Event.ACQUIRE_GOLD);
		}
		
		SaveManager.Instance.saveLevel(newData);
		AchievementManager.Instance.saveStats();
	}
}
