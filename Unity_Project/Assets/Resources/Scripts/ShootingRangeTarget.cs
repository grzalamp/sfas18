﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingRangeTarget : MonoBehaviour, Resetable {

	private float speed = 5.0f;
	[SerializeField] int cost = 100;

	[SerializeField] bool moving = true;

	private Subject subject;

	// Use this for initialization
	void Start () {
		subject = GameManager.Instance.GetSubject();
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
	
	// Update is called once per frame
	void Update () {
		if (moving) {
			transform.Translate(-speed*Time.deltaTime, 0f, 0f);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "DamageByPlayer") {
			GameManager.Instance.addPoints(cost);

			var triggerTarget = GetComponent<TriggerTarget>();
			if (triggerTarget != null) {
				triggerTarget.trigger();
			}

			// object is not destroyed, it is only made uninteractable
			// it will be needed later - if player dies the target must be enabled again
			GetComponentInChildren<Renderer>().enabled = false;
			GetComponentInChildren<Collider>().enabled = false;

			subject.notify(gameObject, null, O_Event.TARGET_SHOT);
		}
	}

	public void reset() {
		// if belongs to shooting range, shooting range will take care of reset
		if (transform.parent.GetComponent<ShootingRange>() == null) {
			GetComponentInChildren<Renderer>().enabled = true;
			GetComponentInChildren<Collider>().enabled = true;
		} else {
			Destroy(gameObject);
		}
	}

}
