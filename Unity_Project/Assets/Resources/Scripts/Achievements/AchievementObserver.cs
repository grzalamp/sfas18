﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementObserver : Observer {

	// handle events and activate achievements
	public override void receive(GameObject entity, GameObject otherEntity, O_Event e) {
		string conditionName;
		switch (e) {
			case O_Event.ENEMY_KILLED:
				conditionName = "enemies_killed";
				AchievementManager.Instance.updateStatistic(conditionName, 1);
				break;
			
			case O_Event.PLAYER_DEATH:
				conditionName = "player_deaths";
            	AchievementManager.Instance.updateStatistic(conditionName, 1);
				break;

			case O_Event.LEVEL_FINISHED:
				conditionName = "finish_level";
				AchievementManager.Instance.triggerAchievement(conditionName, GameManager.Instance.getLevelNo());
				break;			
		
			case O_Event.ACQUIRE_GOLD:
				conditionName = "get_gold";
				AchievementManager.Instance.updateStatistic(conditionName, 1);
				break;			
			
			case O_Event.TARGET_SHOT:
				conditionName = "targets_shot";
				AchievementManager.Instance.updateStatistic(conditionName, 1);
				break;

			case O_Event.COLLECT_HAT:
				conditionName = "collect_hat";
				AchievementManager.Instance.unlockAchievementByConditionName(conditionName);
				break;
				
		}

	}
}
