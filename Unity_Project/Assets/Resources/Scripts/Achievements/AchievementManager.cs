﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public enum AchievementOperator {
	GREATER,
	SMALLER,
	TRIGGER,
	EQUALS,
}

public struct Achievement {
	public string name;
	public string description;

	//condition
	public string conditionName;
	public AchievementOperator op;
	public int amount;

	public bool unlocked;
}

public class AchievementManager : MonoBehaviour {
	private static AchievementManager instance;
	public static AchievementManager Instance {
		get {return instance;}
	}


	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy(gameObject);
		}

		//DontDestroyOnLoad(gameObject);
	}

	private Dictionary<string, StatisticsData> statisticsDB;
	public Dictionary<string, StatisticsData> StatisticsDB {
		get {return statisticsDB;}
	}

	private Dictionary<string, List<Achievement>> achievementDB;
	public Dictionary<string, List<Achievement>> AchievementDB {
		get {return achievementDB;}
	}

	[SerializeField] TextAsset achievementsXML;
	[SerializeField] AchievementDisplay achievementDisplay;

	// Use this for initialization
	void Start () {
		statisticsDB = SaveManager.Instance.getStatisticsData();

		initAchievements();
	}

	private void initAchievements() {
		var unlockedAchievements = SaveManager.Instance.getUnlockedAchievementNames();
        var xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(achievementsXML.text);

		achievementDB = new Dictionary<string, List<Achievement>>();
		var achievements = xmlDoc.SelectSingleNode("achievements").SelectNodes("achievement");
		foreach (XmlNode achievement in achievements) { 
			var data = new Achievement();

			var attrs = achievement.Attributes;
			data.name = attrs["name"].Value;
			data.description = achievement["description"].InnerText;

			var condition = achievement["condition"];
			var operand = condition.Attributes["operator"].Value;
			if (operand == "GREATER") {
				data.op = AchievementOperator.GREATER;
			} else if (operand == "SMALLER") {
				data.op = AchievementOperator.SMALLER;
			} else if (operand == "TRIGGER") {
				data.op = AchievementOperator.TRIGGER;
			} else if (operand == "EQUALS") {
				data.op = AchievementOperator.EQUALS;
			}
			data.conditionName = condition.Attributes["name"].Value;
			data.amount = int.Parse(condition.Attributes["amount"].Value);
			data.unlocked = unlockedAchievements.Contains(data.name);

			if (!achievementDB.ContainsKey(data.conditionName)) {
				achievementDB[data.conditionName] = new List<Achievement>();
			}
			achievementDB[data.conditionName].Add(data);
		}

	}
	
	public void updateStatistic(string conditionName, int value) {
		var stat = StatisticsDB[conditionName];
		stat.amount += value;
		StatisticsDB[conditionName] = stat;
		var relevantAchievements = achievementDB[conditionName];
		for (int i = 0; i < relevantAchievements.Count; ++i) {
			var achievement = relevantAchievements[i];

			var unlock = false;
			if (!achievement.unlocked) {
				if (achievement.op == AchievementOperator.GREATER) {
					unlock = stat.amount > achievement.amount;
				} else if (achievement.op == AchievementOperator.GREATER) {
					unlock = stat.amount < achievement.amount;
				}
			}

			if (unlock) {
				achievement.unlocked = true;
				relevantAchievements[i] = achievement;
				
				//save immediately!
				SaveManager.Instance.saveUnlockAchievement(achievement.name);

				//display
				displayAchievement(achievement);
			}
		}
	}

	public bool triggerAchievement(string conditionName, int value) {
		var achievements = achievementDB[conditionName];
		for (int i = 0; i < achievements.Count; ++i) {
			var achievement = achievements[i];
			if (!achievement.unlocked && achievement.amount == value) {
				achievement.unlocked = true;
				achievementDB[conditionName][i] = achievement;
				
				//save immediately!
				SaveManager.Instance.saveUnlockAchievement(achievement.name);

				//display
				displayAchievement(achievement);
			}
		}

		return false;
	}

	public bool unlockAchievementByConditionName(string conditionName) {
		var achievements = achievementDB[conditionName];
		if (achievements.Count < 1 || achievements[0].unlocked) {
			return false;
		}

		var achievement = achievements[0];
		achievement.unlocked = true;
		achievementDB[conditionName][0] = achievement;
		SaveManager.Instance.saveUnlockAchievement(achievement.name);
		displayAchievement(achievement);


		return true;
	}

	private void displayAchievement(Achievement achievement) {
		achievementDisplay.displayAchievement(achievement);
	}

	public void saveStats() {
		SaveManager.Instance.saveStats(statisticsDB);
	}
}
