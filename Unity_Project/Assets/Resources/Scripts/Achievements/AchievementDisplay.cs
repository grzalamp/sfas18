﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementDisplay : MonoBehaviour {
	private static float EPSILON = 0.005f;
	[SerializeField] Text title, description;

	private Vector3 startPos;
	private float speed = 0.1f;
	private float displayTime = 1.5f;
	private float yOffset = 100f;
	private Queue<Achievement> achievementsQueue = new Queue<Achievement>();
	private bool displayLock = false;

	void Start() {
		startPos = transform.position;
	}

	public void displayAchievement(Achievement achievement) {
		achievementsQueue.Enqueue(achievement);
		if (!displayLock) {
			displayLock = true;
			StartCoroutine(displayCoroutine());
		}
	}

	IEnumerator displayCoroutine() {
		yield return new WaitForEndOfFrame();
		while (achievementsQueue.Count > 0) {
			var achievement = achievementsQueue.Dequeue();
			title.text = achievement.name;
			description.text = achievement.description;

			var targetPos = startPos + new Vector3(0f, yOffset, 0f);
			while(Vector3.Distance(transform.position, targetPos) > EPSILON) {
				transform.position = Vector3.Lerp(transform.position, targetPos, speed);
				yield return new WaitForEndOfFrame();
			}

			yield return new WaitForSecondsRealtime(displayTime);

			while(Vector3.Distance(transform.position, startPos) > EPSILON) {
				transform.position = Vector3.Lerp(transform.position, startPos, speed);
				yield return new WaitForEndOfFrame();
			}
		}

		displayLock = false;
		yield return null;
	}
}
