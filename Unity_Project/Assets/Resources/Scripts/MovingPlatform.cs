﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MovingPlatform : Activateable, Resetable {

	List<Vector3> targets = new List<Vector3>();
	int currentTarget;

	[SerializeField] float speed = 5f;
	[SerializeField] float stopTime = 0.5f;
	[SerializeField] float startDelay = 0f;

	enum PlatformState {
		IDLE,
		STARTUP,
		MOVE,
		WAIT
	}

	private PlatformState state = PlatformState.IDLE;

	float timeWaited = 0f;

	// Use this for initialization
	void Start () {

		foreach (Transform t in GetComponentsInChildren<Transform>()) {
			if (t.tag == "MovingPlatformTarget") {
				targets.Add(t.position);
			}
		}

		Assert.AreNotEqual(targets.Count, 0);
		targets.Add(transform.position);
		currentTarget = 0;
	}
	
	// Update is called once per frame
	void Update () {
		// 1. Wait the initial delay
		// 2. Move to a point
		// 3. Wait cooldown
		// 4. Move to next point. Visit points in loop
		if (state == PlatformState.MOVE) {
			Vector3 movement = Vector3.MoveTowards(transform.position, targets[currentTarget], speed*Time.deltaTime);
			transform.position = movement;
			float distance = Vector3.Distance(transform.position, targets[currentTarget]);
			if (distance < float.Epsilon) {
				currentTarget = (currentTarget + 1) % targets.Count;
				state = PlatformState.WAIT;
			}
		} else if (state == PlatformState.WAIT) {
			timeWaited += Time.deltaTime;
			if (timeWaited > stopTime) {
				timeWaited = 0f;
				state = PlatformState.MOVE;
			}
		} else if (state == PlatformState.STARTUP) {
			timeWaited += Time.deltaTime;
			if (timeWaited > startDelay) {
				timeWaited = 0f;
				state = PlatformState.MOVE;
			}
		}
	}

	public void reset() {
		timeWaited = 0f;
		state = PlatformState.STARTUP;
		if (targets.Count > 0) {
			transform.position = targets[targets.Count - 1];
		}
		currentTarget = 0;
	}

	public override void activate() {
		state = PlatformState.STARTUP;
	}

	public override void deactivate() {
		reset();
		state = PlatformState.IDLE;
	}
}
