﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// upon calling overridable trigger(), object will perform some action
public abstract class ActionTrigerrable : MonoBehaviour {


	public abstract void trigger();

}
