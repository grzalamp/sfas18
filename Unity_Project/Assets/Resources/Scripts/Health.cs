﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// GameObjects with Health component can receive damage and die
public class Health : MonoBehaviour {

	[SerializeField] int health = 100;
	private int maxHealth;

	private bool dead = false;

	public bool invincible = false;


    private AudioSource audioSource;
    [SerializeField] AudioClip hurtSound;

	public bool isDead() {
		return dead;
	}

	// Use this for initialization
	void Start () {
		maxHealth = health;

		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (dead) {
			//print("DED");
		}
	}

	public void heal(int amount) {
		health += amount;
		health = Mathf.Min(health, maxHealth);
	}

	public void hit(int amount) {
		if (invincible || health <= 0) {
			return;
		}
		
		if (audioSource != null) {
			audioSource.PlayOneShot(hurtSound);
		}

		health -= amount;
		if (health <= 0) {
			health = 0;
			dead = true;
		}
	}

	public void restore() {
		health = maxHealth;
		dead = false;
	}

	public int getHealth() {
		return health;
	}
}
