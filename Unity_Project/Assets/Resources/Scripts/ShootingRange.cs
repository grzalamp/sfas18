﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ShootingRange : MonoBehaviour, Resetable {

	[SerializeField] GameObject targetType;
	[SerializeField] bool[] targetSequence;
	[SerializeField] Transform spawnPoint;
	[SerializeField] float speed = 5.0f;
	[SerializeField] float spawnCooldown = 0.5f;
	[SerializeField] int cost = 100;
	[SerializeField] bool looping = true;

	private List<ShootingRangeTarget> targets = new List<ShootingRangeTarget>();

	// Use this for initialization
	void Start () {
		Assert.AreNotEqual(targetSequence.Length, 0);

		StartCoroutine(spawnCoroutine(0));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void spawnTarget() {
		GameObject newTarget = Instantiate(targetType) as GameObject;
		newTarget.transform.parent = transform;
		newTarget.transform.position = spawnPoint.position;
		newTarget.transform.rotation = transform.rotation;
		ShootingRangeTarget newTargetInstance = newTarget.GetComponent<ShootingRangeTarget>();
		newTargetInstance.setSpeed(speed);
		newTargetInstance.setCost(cost);
		targets.Add(newTargetInstance);
	}

	void respawnTarget(ShootingRangeTarget t) {
		t.transform.position = spawnPoint.position;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "ShootingRangeTarget") {
			ShootingRangeTarget t = other.GetComponent<ShootingRangeTarget>();
			if (looping) {
				respawnTarget(t);
			} else {
				targets.Remove(t);
				Destroy(t.gameObject);
			}
		}
	}

	IEnumerator spawnCoroutine(int i) {
		if (i < targetSequence.Length) {
			if (targetSequence[i]) {
				spawnTarget();
			}

			yield return new WaitForSeconds(spawnCooldown);
			StartCoroutine(spawnCoroutine(i+1));
		}
	}

	public void reset() {
		targets = new List<ShootingRangeTarget>();
		foreach (var target in GetComponentsInChildren<ShootingRangeTarget>()) {
			Destroy(target.gameObject);
		}
		StartCoroutine(spawnCoroutine(0));
	}
}
