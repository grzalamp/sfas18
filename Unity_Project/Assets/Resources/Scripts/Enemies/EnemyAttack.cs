﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public abstract class EnemyAttack : MonoBehaviour {

	private bool attacking = false;

	protected GameObject weaponPrefab;
	protected Weapon weaponScript;


	private GameObject hand;
	protected GameObject target;

	protected void onStart() {
		hand = transform.Find("Hand").gameObject;
		Assert.IsNotNull(hand);

		target = GameManager.Instance.getPlayer();
	}

	public bool isAttacking() {
		return attacking;
	}

	public void enterAttackSequence() {
		attacking = true;
	}

	protected void finishAttackSequence() {
		attacking = false;
	}

	protected void spawnWeaponPrefab() {
		var weapon = Instantiate(weaponPrefab) as GameObject;
		weaponScript = weapon.GetComponent<Weapon>();

		weapon.transform.parent = hand.transform;
		weapon.transform.localPosition = new Vector3(0f, 0f, 0f);
		weapon.transform.rotation = new Quaternion();
		weapon.tag = "DamageFromEnemies";
		weaponScript.setAmmoTag("DamageFromEnemies");

	}

	public Weapon getWeapon() {
		return weaponScript;
	}
}
