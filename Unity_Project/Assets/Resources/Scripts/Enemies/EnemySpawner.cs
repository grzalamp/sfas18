﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpawnerEnemyType {
	SHOOTER,
	GRENADIER,
}

public class EnemySpawner : MonoBehaviour, Resetable {
	[SerializeField] bool triggersNextLevel = false;
	static System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

	[SerializeField] float widthX;
	[SerializeField] float widthZ;
	[SerializeField] int numberOfEnemies;
	[SerializeField] GameObject enemyPrefab;


	[SerializeField] SpawnerEnemyType enemyType;

	private bool clearedNotified = false;

	private List<Vector3> spawnPoints = new List<Vector3>();
	private List<Enemy> childEnemies = new List<Enemy>();
	private float minx, maxx, minz, maxz;

	private float minDistance = 2f;

	private Subject observationSubject;

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.blue;
		Vector3 point1 = transform.position - new Vector3(widthX, 0f, widthZ);
		Vector3 point2 = transform.position - new Vector3(-widthX, 0f, widthZ);
		Vector3 point3 = transform.position - new Vector3(-widthX, 0f, -widthZ);
		Vector3 point4 = transform.position - new Vector3(widthX, 0f, -widthZ);
		
		Gizmos.DrawLine(point1, point2);
		Gizmos.DrawLine(point2, point3);
		Gizmos.DrawLine(point3, point4);
		Gizmos.DrawLine(point4, point1);
    }


	// Use this for initialization
	void Start () {
		observationSubject = GameManager.Instance.GetSubject();
		
		if (triggersNextLevel) {
			observationSubject.notify(gameObject, null, O_Event.ENEMYSPAWNER_CREATED);
		}

		spawnEnemies();
	}

	private void spawnEnemies() {
		int seed = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		Random.InitState(seed);

		minx = transform.position.x - widthX;
		maxx = transform.position.x + widthX;
		minz = transform.position.z - widthZ;
		maxz = transform.position.z + widthZ;

		List<Vector3> spawnPoints = new List<Vector3>();
		var y = transform.position.y;

		for (int i = 0; i < numberOfEnemies; i++) {
			Vector3 spawnPoint;
			do {
				var randX = Random.Range(minx, maxx);
				var randZ = Random.Range(minz, maxz);

				spawnPoint = new Vector3(randX, y, randZ);
			} while (!canSpawnHere(spawnPoint));

			var newEnemyObject = Instantiate(enemyPrefab) as GameObject;
			var newEnemy = newEnemyObject.GetComponent<Enemy>();
			newEnemyObject.transform.position = spawnPoint;
			newEnemy.assignAttackScript(enemyType);
			newEnemy.parentSpawner = this;
			spawnPoints.Add(spawnPoint);
			registerEnemy(newEnemy);
		}
	}


	private bool canSpawnHere(Vector3 point) {
		bool canSpawn = true;
		foreach (var otherPoint in spawnPoints) {
			if (Vector3.Distance(point, otherPoint) < minDistance) {
				canSpawn = false;
				break;
			}
		}
		return canSpawn;
	}
	
	public void registerEnemy(Enemy enemy) {
		childEnemies.Add(enemy);
	}

	public void unregisterEnemy(Enemy enemy) {
		GameManager.Instance.addPoints(100);
		childEnemies.Remove(enemy);
		observationSubject.notify(gameObject, null, O_Event.ENEMY_KILLED);
		if (childEnemies.Count <= 0) {
			if (triggersNextLevel) {
				observationSubject.notify(gameObject, null, O_Event.ENEMYSPAWNER_CLEARED);
				clearedNotified = true;
			}
			enabled = false;
		}
	}

	public void reset() {
		if (clearedNotified) {
			clearedNotified = false;
			observationSubject.notify(gameObject, null, O_Event.ENEMYSPAWNER_CREATED);
		}

		foreach (Enemy enemy in childEnemies) {
			Destroy(enemy.gameObject);
		}

		childEnemies.Clear();
		spawnEnemies();
	}
}
