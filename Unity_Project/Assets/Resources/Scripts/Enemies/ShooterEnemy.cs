﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.AI;

public class ShooterEnemy : EnemyAttack {

	private const float throwDelay = 0.5f;
	private const float longThrowDelay = 1.5f;
	private float nextDelay;
	private float currentlyDelayed = 10f;
	private int shotsInSequence = 0;
	private const int bulletInSeries = 4;

	private NavMeshAgent nav;

	// Use this for initialization
	void Start () {
		onStart();
		weaponPrefab = Resources.Load("Prefabs/Weapons/Enemy Gun", typeof(GameObject)) as GameObject;
		Assert.IsNotNull(weaponPrefab);
		spawnWeaponPrefab();
		target = GameManager.Instance.getPlayer();

		nav = GetComponent<NavMeshAgent>();
		nextDelay = throwDelay;
	}
	

	// Update is called once per frame
	void FixedUpdate () {
		if (isAttacking()) {
			if (Vector3.Distance(target.transform.position, transform.position) < getWeapon().getRange()/2f) {
				nav.isStopped = true;
				transform.rotation = Quaternion	.LookRotation(target.transform.position - transform.position);

				if (currentlyDelayed >= nextDelay) {
					weaponScript.Fire();
					++shotsInSequence;
					if (shotsInSequence >= bulletInSeries) {
						shotsInSequence = 0;
						nextDelay = longThrowDelay;
					} else {
						nextDelay = throwDelay;
					}

					currentlyDelayed = 0f;
				} else {
					currentlyDelayed += Time.deltaTime;
				}
			} else {
				nav.isStopped = false;
				nav.SetDestination(target.transform.position);
			}
		}
	}
}
