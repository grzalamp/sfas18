﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

public class Enemy : MonoBehaviour {

	private enum EnemyState {
		ROAM,
		IDLE,
		ATTACK,
		EVADE,
	}

	private EnemyState state;
	private NavMeshAgent nav;
	private Vector3 currentRoamTarget;
	private static float[] POSSIBLE_ANGLES = {0f, Mathf.PI/4, Mathf.PI/2, (3*Mathf.PI)/4, Mathf.PI, (5*Mathf.PI)/4, (6*Mathf.PI)/4, (7*Mathf.PI)/4};
	private static int POSSIBLE_ANGLES_LEN = 8;
	private static float MAX_IDLING_TIME = 5f;
	private static float MIN_IDLING_TIME = 0.8f;
	private static float MAX_WALKING_TIME = 3f;
	private float timeWalked = 0f;
	private float currentlyIdlingFor = 0f;
	private float idledTime = 0f;
	static System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

	private EnemyAttack attackScript;
	private Health healthScript;

	public EnemySpawner parentSpawner = null;

	// Use this for initialization
	void Start () {
		nav = GetComponent<NavMeshAgent>();
		healthScript = GetComponent<Health>();
		nav.Warp(transform.position);
		attackScript = GetComponent<EnemyAttack>();
		Assert.IsNotNull(attackScript); //for some reason it will not work without it

		transitionToRoam();
	}
	
	// Update is called once per frame
	void Update() {
		if (healthScript.isDead()) {
			die();
		}
	}

	void FixedUpdate () {
		switch(state) {
			case EnemyState.ROAM:
				timeWalked += Time.deltaTime;
				var distance = Vector3.Distance(transform.position, currentRoamTarget);
				if (timeWalked > MAX_WALKING_TIME || distance < 0.8f) {
					//transitionToIdle();
					if (attackScript != null) {
						var distanceToPlayer = Vector3.Distance(transform.position, GameManager.Instance.getPlayer().transform.position);
						if (attackScript.getWeapon().getRange() >= distanceToPlayer)
							transitionToAttack();
					} else {
						transitionToRoam();
					}
				}
				break;
			
			case EnemyState.IDLE:
				idledTime += Time.deltaTime;
				if (idledTime > currentlyIdlingFor) {
					transitionToRoam();
				}
				break;

			case EnemyState.ATTACK:
				if(!attackScript.isAttacking()) {
					transitionToIdle();
				}
				break;
		}
	}

	private Vector3 getRandomDestination() {
		int seed = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
		Random.InitState(seed);
		var distance = Random.Range(2f, 10f);
		var angle = POSSIBLE_ANGLES[Random.Range(0, POSSIBLE_ANGLES_LEN)];

		var destination = new Vector3(Mathf.Cos(angle)*distance, 0f, Mathf.Sin(angle)*distance);
		destination += transform.position;
		
		return destination;
	}

	private void transitionToRoam() {
		nav.isStopped = false;
		state = EnemyState.ROAM;
		currentRoamTarget = getRandomDestination();
		nav.SetDestination(currentRoamTarget);
		int i = 0;
		while (nav.pathStatus == NavMeshPathStatus.PathInvalid) {
			currentRoamTarget = getRandomDestination();
			nav.SetDestination(currentRoamTarget);
			i++; //if for whatever reason the correct path cant be found, give up
			if (i >= 20) {
				transitionToIdle();
				return;
			}
		}; 
		timeWalked = 0f;
	}


	private void transitionToIdle() {
		nav.isStopped = true;
		state = EnemyState.IDLE;
		idledTime = 0f;

		currentlyIdlingFor = Random.Range(MIN_IDLING_TIME, MAX_IDLING_TIME);
	}

	private void transitionToAttack() {
		nav.isStopped = true;
		state = EnemyState.ATTACK;
		attackScript.enterAttackSequence();
	}

	public void assignAttackScript(SpawnerEnemyType type) {
		switch(type) {
			case SpawnerEnemyType.SHOOTER: 
				gameObject.AddComponent<ShooterEnemy>();
				attackScript = GetComponent<EnemyAttack>();

				break;
			case SpawnerEnemyType.GRENADIER: 
				gameObject.AddComponent<GrenadierEnemy>();
				attackScript = GetComponent<EnemyAttack>();

				break;
		}
	}

	private void die() {
		if (parentSpawner != null) {
			parentSpawner.unregisterEnemy(this);
		}
		Destroy(gameObject);
	}
}
