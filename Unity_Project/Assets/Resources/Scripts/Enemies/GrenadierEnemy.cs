﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GrenadierEnemy : EnemyAttack {

	private float throwDelay = 1.5f;
	private float currentlyDelayed = 10f;
	private int throwsInSequence = 4;

	private int currentThrow = 0;

	// Use this for initialization
	void Start () {
		onStart();
		weaponPrefab = Resources.Load("Prefabs/Weapons/Grenade Gun", typeof(GameObject)) as GameObject;
		Assert.IsNotNull(weaponPrefab);
		spawnWeaponPrefab();
		target = GameManager.Instance.getPlayer();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (isAttacking()) {

			transform.rotation = Quaternion	.LookRotation(target.transform.position - transform.position);

			if (currentlyDelayed > throwDelay) {
				weaponScript.Fire();
				currentThrow++;
				currentlyDelayed = 0f;
			} else {
				currentlyDelayed += Time.deltaTime;
			}

			if (throwsInSequence <= currentThrow) {
				finishAttackSequence();
				currentThrow = 0;
				currentlyDelayed = 10f;
			}

		}
		
	}
}
