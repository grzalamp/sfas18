﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this script will allow transparent walls so that they do not obstruct player's view
public class SeeThroughable : MonoBehaviour {

	[SerializeField] Material transparentMaterial;
	private Material normalMaterial;

	private bool dimmed = false;
	
	private const float maxTransparency = 1f;
	private const float minTransparency = 0.6f;
	private const float dimSpeed = 0.3f;
	private const float undimDelay = 0.3f;
	private float currentlyDelayed = 0f;
	private new Renderer renderer;

	// Use this for initialization
	void Start () {
		renderer = GetComponent<Renderer>();
		normalMaterial = renderer.material;

		StartCoroutine(delayCoroutine());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void dim() {
		currentlyDelayed = 0f;

		if (!dimmed) {
			dimmed = true;
			StartCoroutine(dimCoroutine());
		}
	}

	public void undim() {
		if (dimmed) {
			dimmed = false;
			StartCoroutine(undimCoroutine());
		}
	}
	
	private IEnumerator delayCoroutine() {
		while(true) {
			currentlyDelayed += Time.deltaTime;

			if (currentlyDelayed >= undimDelay) {
				undim();
			}

			yield return null;
		}
	}

	private IEnumerator dimCoroutine() {
		renderer.material = transparentMaterial;
		var currentDim = renderer.material.color.a;
		var oldColor = renderer.material.color;
		while (dimmed && currentDim > minTransparency) {
			currentDim = renderer.material.color.a;
			oldColor = renderer.material.color;
			var newDim = Mathf.Min(minTransparency, currentDim + (-dimSpeed*Time.deltaTime));
			renderer.material.color = new Color(oldColor.r, oldColor.g, oldColor.b, newDim);
			yield return null;
		}

		yield return null;
	}
	private IEnumerator undimCoroutine() {
		renderer.material = normalMaterial;
		var currentDim = renderer.material.color.a;
		var oldColor = renderer.material.color;
		while (!dimmed && currentDim < maxTransparency) {
			currentDim = renderer.material.color.a;
			oldColor = renderer.material.color;
			var newDim = Mathf.Max(maxTransparency, currentDim + (dimSpeed*Time.deltaTime));
			renderer.material.color = new Color(oldColor.r, oldColor.g, oldColor.b, newDim);
			yield return null;
		}

		yield return null;
	}
}
