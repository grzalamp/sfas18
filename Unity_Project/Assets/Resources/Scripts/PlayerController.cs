﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    // --------------------------------------------------------------

    // The character's running speed
    [SerializeField]
    float m_RunSpeed = 5.0f;

    // The gravity strength
    [SerializeField]
    float m_Gravity = 60.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;

    // The character's jump height
    [SerializeField]
    float m_JumpHeight = 4.0f;

    Weapon currentWeapon;


    // --------------------------------------------------------------

    // The charactercontroller of the player
    CharacterController m_CharacterController;

    // The current movement direction in x & z.
    Vector3 m_MovementDirection = Vector3.zero;

    // The current movement speed
    float m_MovementSpeed = 0.0f;
    private float speedMultiplier = 1f;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // The current movement offset
    Vector3 m_CurrentMovementOffset = Vector3.zero;

    // The starting position of the player
    //Vector3 m_SpawningPosition = Vector3.zero;

    // Whether the player is alive or not
    bool m_IsAlive = true;

    // The time it takes to respawn
    const float MAX_RESPAWN_TIME = 1.0f;
    float m_RespawnTime = MAX_RESPAWN_TIME;

    private float coyoteJumpTimeLimit = 0.1f;
    private float timeInAir = 0f;
    private bool jumped = false;

    private Health healthScript;
    private Subject observationSubject = null;
    private Transform handPosition;
    private Transform hatPosition;

    public bool controlsLocked = false;
	[SerializeField] LayerMask layerMaskProjectileRay;

    [SerializeField] Light targetProjector;

    // --------------------------------------------------------------

    void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    // Use this for initialization
    void Start()
    {
        //those hold guns and hats
        handPosition = transform.Find("Hand").transform;
        hatPosition = transform.Find("Hat Bone").transform;
        //m_SpawningPosition = transform.position;
        currentWeapon = GetComponentInChildren<Weapon>();


        observationSubject = GameManager.Instance.GetSubject();//GetComponent<Subject>();
        healthScript = GetComponent<Health>();
        Assert.IsNotNull(observationSubject);
        Assert.IsNotNull(healthScript);

        targetProjector.enabled = false;

    }

    void Jump()
    {
        m_VerticalSpeed = Mathf.Sqrt(m_JumpHeight * m_Gravity);
        m_VerticalSpeed *= speedMultiplier;
        jumped = true;
    }

    public void Knockback(Vector3 vec) {
        StartCoroutine(KnockbackCoroutine(vec));
    }

    // Knockback without rigidbody
    IEnumerator KnockbackCoroutine(Vector3 vec) {
        while (vec.sqrMagnitude > 0.01f) {
            m_CharacterController.Move(vec * Time.deltaTime);
            vec *= 0.8f;
            yield return null;
        }

        yield return null;
    }

    void ApplyGravity()
    {
        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    }

    void UpdateMovementState()
    {
        if (!controlsLocked) {
            // Get Player's movement input and determine direction and set run speed
            float horizontalInput = Input.GetAxisRaw("Horizontal");
            float verticalInput = Input.GetAxisRaw("Vertical");

            m_MovementDirection = new Vector3(horizontalInput, 0, verticalInput);
            m_MovementSpeed = m_RunSpeed * speedMultiplier;
        }
    }

    void UpdateJumpState()
    {
        // Character can jump when standing on the ground
        if (!controlsLocked && Input.GetButtonDown("Jump") && (m_CharacterController.isGrounded || (!jumped && coyoteJumpTimeLimit > timeInAir)))
        {
            Jump();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (healthScript.isDead() && m_IsAlive) {
            Die();
        }

        // If the player is dead update the respawn timer and exit update loop
        if(!m_IsAlive)
        {
            UpdateRespawnTime();
            return;
        }

        // Update movement input
        UpdateMovementState();

        // Update jumping input and apply gravity
        UpdateJumpState();
        ApplyGravity();

        // Fire weapon
        if (!controlsLocked && Input.GetButton("Fire1")) {
            currentWeapon.Fire();
        }

        // Calculate actual motion
        m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;

        // Move character
        m_CharacterController.Move(m_CurrentMovementOffset);

        // Rotate the character in movement direction
        if(m_MovementDirection != Vector3.zero)
        {
            RotateCharacter(m_MovementDirection);
        }

        if  (!m_CharacterController.isGrounded) {
            timeInAir += Time.deltaTime;
        } else {
            timeInAir = 0f;
            jumped = false;
        }

        displayProjectileRange();
    }

    // show greanade target. Cast raycast from above, vertically down
    private void displayProjectileRange() {
        if (currentWeapon == null) { 
            targetProjector.enabled = false;
        }

        if (currentWeapon.getType() == WeaponType.PARABOLE_PROJECTILE) {
            targetProjector.enabled = true;
            var targetPos = transform.position + transform.forward*currentWeapon.getRange();
            var maxH = currentWeapon.getMaxProjectileHeight();
            var rayStart = targetPos + new Vector3(0f, maxH*1.2f, 0f);
            var rayLength = maxH*3f;

			RaycastHit hit;
			Ray ray = new Ray(rayStart, Vector3.down);
			if (Physics.Raycast(ray, out hit, rayLength, layerMaskProjectileRay)) {
                Debug.DrawLine(ray.origin, hit.point, Color.red);
                targetProjector.transform.position = hit.point + new Vector3(0f, transform.position.y+4.20f, 0f);
			}

        } else {
            targetProjector.enabled = false;
        }
    }

    public void Stop() {
        m_MovementSpeed = 0f;
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    public int GetPlayerNum()
    {
        return 1;
    }

    public void Die()
    {
        m_IsAlive = false;
        m_RespawnTime = MAX_RESPAWN_TIME;
    }

    void UpdateRespawnTime()
    {
        m_RespawnTime -= Time.deltaTime;
        if (m_RespawnTime < 0.0f)
        {
            Respawn();
        }
    }
    void Respawn()
    {
        m_IsAlive = true;
        healthScript.restore();
        observationSubject.notify(gameObject, null, O_Event.PLAYER_RESPAWNED);
        GameManager.Instance.respawnPlayer();
        //transform.position = m_SpawningPosition;
        //transform.rotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
    }

    public int getHealth() {
        return healthScript.getHealth();
    }
    
   void OnTriggerEnter(Collider other)
   {
       if (other.tag == "Room End") {
           if (observationSubject != null) {
               observationSubject.notify(gameObject, other.gameObject, O_Event.PLAYER_REACHED_ROOM_END);
           }
       } else if (other.tag == "TutorialTrigger") {
            other.gameObject.SetActive(false);
            ConversationManager.Instance.startNextEncounter();
       } else if (other.tag == "Water" || other.tag == "FireHazard") {
            if (observationSubject != null) {
                observationSubject.notify(gameObject, other.gameObject, O_Event.PLAYER_DEATH);
                observationSubject.walkObservers();
            }
       } else if (other.tag == "Pickable") {
            Pickable picked = other.GetComponent<Pickable>();
            if (!picked.isPicked()) {
                pickUp(picked);
            }
       }
   }

   void pickUp(Pickable pickable) {
        var type = pickable.getType();

        if (type == PickableType.WEAPON) {
            currentWeapon.drop();
            GameObject newWeaponObject = Instantiate(pickable.getWeapon()) as GameObject;
            newWeaponObject.transform.parent = handPosition;
            newWeaponObject.transform.localPosition = new Vector3(0f, 0f, 0f);
            newWeaponObject.transform.localRotation = new Quaternion();

            currentWeapon = newWeaponObject.GetComponent<Weapon>();
        } else if (type == PickableType.HAT) {
            speedMultiplier = 2f;
            healthScript.invincible = true;

            var newHat= Instantiate(pickable.getWeapon()) as GameObject;
            newHat.transform.parent = hatPosition;
            newHat.transform.localRotation = new Quaternion();
            newHat.transform.localPosition = Vector3.zero;

            observationSubject.notify(gameObject, null, O_Event.COLLECT_HAT);

        }

        pickable.pick();
   }


}