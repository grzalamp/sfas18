﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ammo : MonoBehaviour {

	public abstract void Fire(GameObject gun, GameObject wielder, Transform firePos, float power);
	public void setTag(string name) {
		tag = name;
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild(i).tag = name;
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
