﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Ammo {

	private Light blinker1, blinker2;

	private const float armTime = 2f;
	private const float explodeTime = 1.2f;
	private bool armed = false;
	[SerializeField] int damage = 60;

	[SerializeField] GameObject explosion;

	public override void Fire(GameObject gun, GameObject wielder, Transform firePos, float power) {

	}

	// Use this for initialization
	void Start () {
		blinker1 = transform.Find("Blinker1").GetComponent<Light>();
		blinker2 = transform.Find("Blinker2").GetComponent<Light>();
		blinker1.enabled = false;
		blinker2.enabled = false;

		StartCoroutine(BlinkCoroutine());
		StartCoroutine(ArmCountdown());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator BlinkCoroutine() {
		if (!armed) {
			blinker2.enabled = false;
			blinker1.enabled = true;
			yield return new WaitForSeconds(0.2f);
			blinker1.enabled = false;
			yield return new WaitForSeconds(0.2f);
		} else {
			blinker1.enabled = false;
			blinker2.enabled = true;
			yield return new WaitForSeconds(0.1f);
			blinker2.enabled = false;
			yield return new WaitForSeconds(0.1f);
		}

		StartCoroutine(BlinkCoroutine());
		yield return null;
	}

	IEnumerator ArmCountdown() {
		yield return new WaitForSeconds(armTime);
		armed = true;
		StartCoroutine(explodeCountdown());
		yield return null;
	}

	IEnumerator explodeCountdown() {
		yield return new WaitForSeconds(explodeTime);
		GameObject newExplosion = Instantiate(explosion) as GameObject;
		newExplosion.transform.position = transform.position;
		newExplosion.transform.rotation = transform.rotation;
		newExplosion.transform.localScale = transform.localScale;
		newExplosion.tag = tag;
		newExplosion.GetComponent<Explosion>().damage = damage;

		Destroy(gameObject);
		yield return null;
	}
}
