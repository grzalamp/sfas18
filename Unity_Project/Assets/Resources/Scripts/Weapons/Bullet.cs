﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Ammo {

	private const float lifetime = 10f;

	private AudioSource audioSource;
	[SerializeField] AudioClip hitSound;

	private float destroyDelay = 0.5f;

	[SerializeField] int damage = 50;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();

		StartCoroutine(lifetimeRoutine());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void Fire(GameObject gun, GameObject wielder, Transform firePos, float power) {
		transform.position = firePos.position;
		transform.rotation = wielder.transform.rotation;
		GetComponent<Rigidbody>().velocity = wielder.GetComponent<CharacterController>().velocity + transform.forward*power;
	}

	void OnTriggerEnter(Collider other) {
		bool hit = (tag == "DamageByPlayer" && other.tag != "Player") || (tag == "DamageFromEnemies" && other.tag != "Enemy");
		if (hit) {
			StartCoroutine(hitObject(other.gameObject));
		}
	}

	void OnCollisionEnter(Collision other) {
		bool hit = (tag == "DamageByPlayer" && other.collider.tag != "Player") || (tag == "DamageFromEnemies" && other.collider.tag != "Enemy");
		if (hit) {
			StartCoroutine(hitObject(other.gameObject));
		}
	}

	private IEnumerator hitObject(GameObject other) {
		var otherHealth = other.GetComponent<Health>();
		if (otherHealth != null) {
			otherHealth.hit(damage);
		}

		if (audioSource != null && hitSound != null) {
			audioSource.PlayOneShot(hitSound);
		}
		GetComponentInChildren<Renderer>().enabled = false;
		GetComponentInChildren<Collider>().enabled = false;
		yield return new WaitForSeconds(destroyDelay);
		Destroy(gameObject);
		yield return null;
	}

	IEnumerator lifetimeRoutine() {
		yield return new WaitForSeconds(lifetime);
		Destroy(gameObject);
		yield return null;
	}
}
