﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public enum WeaponType {
	PARABOLE_PROJECTILE,
	SHOOT_PROJECTILE
}

public class Weapon : MonoBehaviour {

	[SerializeField] string ammoTag = "DamageByPlayer";
	[SerializeField] GameObject AmmoPrefab;
	[SerializeField] float power = 6.0f;
	[SerializeField] float cooldown = 0.15f;

	[SerializeField] AudioClip weaponSound;
	private AudioSource audioSource;

	private Transform firePos;

	[SerializeField] WeaponType type;

	private float range, maxProjectileHeight;

	float cooled = float.MaxValue;

	
	float shotAngle = Mathf.PI/3;

	// Use this for initialization
	void Start () {
		firePos = transform.Find("Fire Position");
		Assert.IsNotNull(firePos);

		audioSource = GetComponent<AudioSource>();

		calculateRange();
		calculateMaxHeight();
	}

	private void calculateRange() {
		if (type == WeaponType.SHOOT_PROJECTILE) {
			range = 50f;
		} else if (type == WeaponType.PARABOLE_PROJECTILE) {
			var optAngle = Mathf.PI/4f;
			range = (power*power*Mathf.Sin(2f*optAngle))/Mathf.Abs(Physics.gravity.y);
		}
	}
	
	private void calculateMaxHeight() {
		if (type == WeaponType.SHOOT_PROJECTILE) {
			maxProjectileHeight = 0f;
		} else if (type == WeaponType.PARABOLE_PROJECTILE) {
			maxProjectileHeight = (power*power*Mathf.Pow(Mathf.Sin(shotAngle), 2))/(2*Mathf.Abs(Physics.gravity.y));
		}
	}


	public float getRange() {
		return range;
	}

	public WeaponType getType() {
		return type;
	}

	public float getMaxProjectileHeight() {
		return maxProjectileHeight;
	}
	
	// Update is called once per frame
	void Update () {
		cooled += Time.deltaTime;
	}

	public void Fire() {
		if (cooled > cooldown) {
			GameObject newAmmoObject = Instantiate(AmmoPrefab) as GameObject;
			Ammo newAmmo = newAmmoObject.GetComponent<Ammo>();
			newAmmo.setTag(ammoTag);

			if (type == WeaponType.PARABOLE_PROJECTILE) {
				FireProjectile(newAmmo, transform.parent.parent.gameObject);
			} else if (type == WeaponType.SHOOT_PROJECTILE) {
				FireBullet(newAmmo, transform.parent.parent.gameObject);
			}

			//newAmmo.Fire(gameObject, transform.parent.parent.gameObject, firePos, power);

			fireSound();

			cooled = 0.0f;
		}
	}

	private void FireProjectile(Ammo ammo, GameObject wielder) {
		ammo.transform.localPosition = new Vector3(0f, 0f, 0f);
		ammo.transform.position = firePos.position;
		ammo.transform.rotation = wielder.transform.rotation;


		var impulse = ammo.transform.forward * power*Mathf.Cos(shotAngle);
		impulse.y = power*Mathf.Sin(shotAngle);

		Vector3 velocity;
		var rigidbody = ammo.GetComponentInChildren<Rigidbody>();
		var wielderController = wielder.GetComponent<CharacterController>();
		if (wielderController != null) {
			velocity = wielder.GetComponent<CharacterController>().velocity;
		} else {
			velocity = wielder.GetComponent<Rigidbody>().velocity;
		}

		//rigidbody.AddRelativeForce(velocity, ForceMode.Impulse);
		rigidbody.AddRelativeForce(impulse, ForceMode.Impulse);

		var angVel = new Vector3(Random.Range(0f, 10f), Random.Range(0f, 10f), Random.Range(0f, 10f));
		rigidbody.angularVelocity = angVel;
	}

	private void FireBullet(Ammo ammo, GameObject wielder) {
		ammo.transform.position = firePos.position;
		ammo.transform.rotation = wielder.transform.rotation;
		
		var vel = Vector3.zero;
		var rb = wielder.GetComponent<CharacterController>();
		if (rb != null) {
			vel = rb.velocity;
		} else { 
			var cc =wielder.GetComponent<CharacterController>();
			if (cc != null) {
				vel = cc.velocity;
			}
		}
		ammo.GetComponent<Rigidbody>().velocity = vel + ammo.transform.forward*power;
	}

	private void fireSound() {
		if (audioSource != null) {
			if (weaponSound != null) {
				audioSource.PlayOneShot(weaponSound);
			}
		}
	}

	public void setPower(float power) {
		this.power = power;
	}

	public void drop() {
		Destroy(gameObject);
	}

	public void setAmmoTag(string t) {
		ammoTag = t;
	}
}
