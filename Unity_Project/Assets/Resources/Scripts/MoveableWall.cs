﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

// wall that will move to a given position when requested
public class MoveableWall : ActionTrigerrable, Resetable {

	private bool moving = false;

	[SerializeField] float speed = 5f;
	private Vector3 target;
	private Vector3 startPos;

	// Use this for initialization
	void Start () {
		var targettransform = transform.Find("Target");
		Assert.IsNotNull(targettransform);
		target = targettransform.position;
		startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (moving) {
			Vector3 movement = Vector3.MoveTowards(transform.position, target, speed*Time.deltaTime);
			transform.position = movement;
			float distance = Vector3.Distance(transform.position, target);
			if (distance < float.Epsilon) {
				moving = false;
			}
		}
	}

	public override void trigger() {
		moving = true;
	}	
	
	public void reset() {
		moving = false;
		transform.position = startPos;
	}

}
