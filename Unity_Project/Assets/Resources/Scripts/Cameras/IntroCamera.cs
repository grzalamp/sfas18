﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroCamera : MonoBehaviour {

	private bool ended = false;

	void Start() {
		string clipName;
		int levelNo = GameManager.Instance.getLevelNo();
		if (levelNo == 0) {
			clipName = "TutorialLevel";
		} else {
			clipName = "Level" + levelNo.ToString();
		}
		
		GetComponent<Animator>().Play(clipName);
	}

	public bool isEnded() {
		return ended;
	}

	public void OnAnimationEnd() {
		ended = true;
	}

	public void Skip() {
		OnAnimationEnd();
	}
}
