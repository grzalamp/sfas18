﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSeeThrough : MonoBehaviour {

	[SerializeField] LayerMask layerMask;

	[SerializeField] GameObject target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
			var rayOrigin = transform.position;
			var direction = (target.transform.position + new Vector3(0f, -0.8f, 0f)) - rayOrigin;
			direction.Normalize();
			var rayLen = 700;

			RaycastHit hit;
			Ray ray = new Ray(rayOrigin, direction);
			Debug.DrawRay(ray.origin, ray.direction * rayLen, Color.red);
			if (Physics.Raycast(ray, out hit, rayLen, layerMask)) {
				var seeThroughable = hit.collider.GetComponent<SeeThroughable>();
				if (seeThroughable != null) {
					seeThroughable.dim();
				}
			}
	}
}
