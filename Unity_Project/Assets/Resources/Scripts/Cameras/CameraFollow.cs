﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class CameraFollow : MonoBehaviour {

	[SerializeField] Transform target;
	[SerializeField] float smoothing = 3.5f;

	[SerializeField] Vector3 offset = new Vector3(5.0f, 8.5f, -10.0f);
	[SerializeField] Vector3 rotationOffset = new Vector3(0f, 0f, 0f);

	void Awake() {
		Assert.IsNotNull(target);
	}

	// Use this for initialization
	void Start () {
		Vector3 targetCamPos = target.position + offset;
		transform.position = targetCamPos;
		transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position);
		transform.Rotate(rotationOffset);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 targetCamPos = target.position + offset;
		transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
	}
}
